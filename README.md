# Solarus UWP Shell

*The UWP port* is a custom made shell around the Solarus game engine.

To have it running you'll need Visual Studio 2017 with UWP C++ developer tools installed

# Building

It won't work with https so use ssh!

You'll need python installed and usable from the PATH, version tested is 3.8
I recommend installing by using chocolatey and pyenv-win

1. git clone git@gitlab.com:balancedwolf/solarus-uwp.git

2. cd solarus-uwp
3. git submodule init 
4. git submodule update
5. Open WinRT/Solarus-WinRT.sln
6. Select the x86 or x64 configuration, ARM and ARM64 are still in development

A) Choose to build Solarus-WinRT if you want to embed your quest and make use of OpenGL

B) You can choose Solarus-WinRT-XAML to load quests on the fly using the XAML loader, it doesn't have shader support

For embedded quests, just as in A), you'll need to manually embed your quest into the Assets folder,
then, load it in place instead of dummy_quest.solarus inside SolarusMain.cpp replacing
std::string quest_path = "Assets/dummy_quest.solarus"; with your own quest. Don't forget to set your quest Content property to true.

Note: Debug builds might be slow because of luajit and other UWP constraints (at least they are slow with my development environment) so I advice to build as Release, it's not that you'll be using development other than to debug internal things.


# Support and Contact
Best way of contact is to open an issue directly, I'll look into it and we'll find out what can we do

Right now the mainly tested game is VOADI, you can test other games and open an issue if you find a bug

Please report all bugs.