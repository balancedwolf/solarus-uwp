#ifndef WINRT_CUSTOM_PRINT_H
#define WINRT_CUSTOM_PRINT_H

#ifdef __cplusplus
extern "C" {
#endif
	int winrt_print(const char* c_string);
#ifdef __cplusplus
}
#endif



#endif