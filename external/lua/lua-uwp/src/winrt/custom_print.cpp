#include "custom_print.h"


#if defined(WINRT)
#include <sstream>
#include <string>
#include <windows.h>
#endif

#ifdef __cplusplus
extern "C" {
#endif
	int winrt_print(const char* c_string) {

		std::stringstream str;

		str << c_string;

		std::string final_message = str.str();

		std::wstring log_message(final_message.begin(), final_message.end());

		OutputDebugString(log_message.c_str());

		return 0;
	}

#ifdef __cplusplus
}
#endif


